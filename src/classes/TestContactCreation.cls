/**
* @File Name:   TestContactCreation.cls
* @Description:
* @Author:      Gopal Kanniappan
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2019-09-21  Gopal Kanniappan    Created the file/class
*/

@isTest
public class TestContactCreation {
    static testMethod void testContactCreationFromAcc() {

        Test.startTest();

        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < 100; i++) {
            Account a = new Account();
            a.Name = 'DeloitteAssignment '+ i;
            accs.add(a);
        }
        insert accs;

        List<Contact> cons = new List<Contact>();
        cons = [SELECT Id, LastName, Account.Name
                    FROM Contact
                    where LastName like 'DeloitteAssignment%'];

        System.assertEquals(100,cons.size());

        Test.stopTest();
    }
}