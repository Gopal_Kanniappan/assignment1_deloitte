/**
* @File Name:   CreateContactFromAccount.cls
* @Description:
* @Author:      Gopal Kanniappan
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2019-09-21  Gopal Kanniappan    Created the file/class
*/
public with sharing class CreateContactFromAccount {
 public static void createContact(list<Account> accounts){
     list<Contact> contacts_ls=new list<Contact>();
     for(Account acc : accounts){
         Contact con = new Contact(LastName = acc.Name, AccountId = acc.Id);
         contacts_ls.add(con);
     }
     insert contacts_ls;
 }
}