/**
* @File Name:   NF_AccountTrigger.trigger
* @Description:
* @Author:      Gopal Kanniappan
* @Group:       Trigger
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2019-09-21  Gopal Kanniappan    Created the file/Trigger
*/
trigger NF_AccountTrigger on Account (
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete ) {
    NF_TriggerFactory.CreateHandlerAndExecute(Account.sObjectType);
}